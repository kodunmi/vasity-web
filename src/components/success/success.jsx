import React, { useContext } from 'react'
import { Context } from '../../store';
import './success.scss'
const Success = ({show , children}) => {

    const [state, dispatch] = useContext(Context);

    const showHideClassName = state.success ? "d-block" : "d-none";
    return (
        <div className={`sucess-modal ${showHideClassName}`}>
            {children}
        </div>
    )
}

export default Success
