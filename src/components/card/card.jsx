import React from 'react'
import Button from '../button/button'
import './card.scss'

const Card = ({ image, name, location, type, experience }) => {
    return (
        <div className='card'>
            <img src={image} alt="" />
            <p className='mb1 name'>{name}</p>
            <div className="detail mb2">
                <p className="location mr1">in {location}</p>
                {
                    type === 'customer' ? <Button size='small' color='#0D019A' background='secondary'>{type}</Button> : <Button size='small' color='#049A01' background='info'>{type}</Button>
                }
                
            </div>
            <p className="exp">{experience}</p>
        </div>
    )
}

export default Card
