import React from 'react'

const Icon = ({color , children, background}) => {
    return (
        <div style={{padding: '10px', borderRadius:'50%', backgroundColor:background, color: color , display:'inline-block'}}>
            {children}
        </div>
    )
}

export default Icon
