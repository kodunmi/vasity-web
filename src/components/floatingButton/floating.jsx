import React from 'react'
import './floating.scss'

const Floating = ({color, text,onClick}) => {
    return (
        <div onClick={onClick} className='floating' style={{backgroundColor:color}}>
            {text}
        </div>
    )
}

export default Floating
