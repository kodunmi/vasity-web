import React from 'react'

const Button = ({ size, outline, color, background, children , onclick,disable}) => {
    let font = size == 'large' ? '20px' : size == 'xlarge' ? '20px' : '10px'
    let padding = size == 'large' ? '10px 20px' : size == 'xlarge' ? '10px 70px'  : '5px 10px'
    let backgroundcolor = background == 'primary' ? '#FF5C00' : 'secondary' ? '#EEF8FF' : '#F0FFEE'

    if(disable){
        return (
            <div  style={{ cursor:'pointer' ,fontSize: font, backgroundColor: ' #d3d3d3', color: color, padding: padding, borderRadius: '5px', display: 'inline-block' , cursor:"unset" }}>
                {children}
            </div>
        )
    }else{
        if (!outline) {
        return (
            <div  onClick={onclick} style={{ cursor:'pointer' ,fontSize: font, backgroundColor: backgroundcolor, color: color, padding: padding, borderRadius: '5px', display: 'inline-block' }}>
                {children}
            </div>
        )
    } else {
        return (
            <div onClick={onclick} style={{ cursor:'pointer' ,fontSize: font, color: color, padding: padding, borderRadius: '5px', display: 'inline-block', border: `1px solid ${color}` }}>
                {children}
            </div>
        )

    }

    }
    



}

export default Button
