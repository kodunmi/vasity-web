import React from 'react'
import './spinner.scss'
const Spinner = ({color}) => {
    return (
        <div className='spinner' style={{borderTop:`4px solid ${color}`}}>
            
        </div>
    )
}

export default Spinner
