import React, { useContext } from 'react'
import { Context } from '../../store';
import './modal.scss'



const Modal = ({hanleClose, children}) => {

    const [state, dispatch] = useContext(Context);
    
    const showHideClassName = state.show ? "modal d-block" : "modal d-none";
    return (
        <div className={showHideClassName}>
           
            <div className="modal-content"> 
            <span onClick={() => dispatch({type:"TOGGLE_MODAL"})} class="close">&times;</span>
                {children}
            </div>
            
        </div>
    )
}

export default Modal
