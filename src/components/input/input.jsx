import React, { useState } from 'react'
import './input.scss'
import {convertToBase64} from '../../utiles'


const Input = ({ text, type, onChange }) => {

    const [file, setFile] = useState(false)
    
    const handleChange = async (file)=>{
        let base64 = ''
        setFile(file)
        if(file) base64 = await convertToBase64(file)
        
        if(type === 'file') onChange(base64)
    }


    return (
        <div>

            <div style={{ fontSize: '13px', marginBottom: '10px' }}>{text}</div>

            {
                type == 'file' ? <div class="box">
                    
                    <input type="file" id="file" onChange={e => handleChange(e.target.files[0])} class="inputfile" />
                    <label className='label' for="file">
                        <span>{file ? file.name : 'choose a file'}</span>
                        {
                            file ? <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18 6L6 18" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M6 6L18 18" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                             : <svg style={{marginLeft:"10px"}} width="22" height="23" viewBox="0 0 22 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20.44 11.0499L11.25 20.2399C10.1242 21.3658 8.59723 21.9983 7.00505 21.9983C5.41286 21.9983 3.88589 21.3658 2.76005 20.2399C1.6342 19.1141 1.00171 17.5871 1.00171 15.9949C1.00171 14.4027 1.6342 12.8758 2.76005 11.7499L11.95 2.55992C12.7006 1.80936 13.7186 1.3877 14.78 1.3877C15.8415 1.3877 16.8595 1.80936 17.61 2.55992C18.3606 3.31048 18.7823 4.32846 18.7823 5.38992C18.7823 6.45138 18.3606 7.46936 17.61 8.21992L8.41005 17.4099C8.03476 17.7852 7.52577 17.996 6.99505 17.996C6.46432 17.996 5.95533 17.7852 5.58005 17.4099C5.20476 17.0346 4.99393 16.5256 4.99393 15.9949C4.99393 15.4642 5.20476 14.9552 5.58005 14.5799L14.07 6.09992" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                        }
                        
                    </label>
                </div> :
                
                type === 'textarea' ? <textarea onChange={onChange} className='input' type='textarea' rows='6' ></textarea> : <input onChange={onChange} className='input' type={type} />



            }

            {/* <div style={{ fontSize: '13px', marginBottom: '10px' }}>{text}</div> */}
            {/* 
            {
                type === 'textarea' ? <textarea onChange={onChange} className='input' type='textarea' rows='6' ></textarea> : <input onChange={onChange} className='input' type={type} />
            } */}


            {/* <div class="box">
                <input type="file" id="file-2" class="inputfile inputfile-2" />
                <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" /></svg> <span>Choose a file&hellip;</span></label>
            </div> */}

        </div>
    )
}

export default Input
