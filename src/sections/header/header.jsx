import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom';
import Button from '../../components/button/button'
import { Context } from '../../store';
import './header.scss'

const Header = ({}) => {
    const history = useHistory()
    const [state, dispatch] = useContext(Context);
    return (
        <div className='header'> 
            <div className='logo' onClick={() => history.push('/')}>
                <img src="/logo.png" alt=""/>
            </div>
            <div className='header-links-container'>
                <ul className="header-links">
                <li  onClick={() => history.push('/about')}>ABOUT US</li>
                <li onClick={() => history.push('/stories')}>STORIES</li>
                <li>CONTACT</li>
                <li>LOG IN</li>
                
            </ul>
            <Button onclick={() => dispatch({type:'TOGGLE_MODAL'})} color='white' size='large' background='primary'>Share Story</Button>
            </div>
            
        </div>
    )
}

export default Header
