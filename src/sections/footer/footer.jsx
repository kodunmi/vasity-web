import React from 'react'
import './footer.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import Icon from '../../components/icon/icon'

const Footer = () => {
    return (
        <div className='footer'>
            <div className="top">
                <img src="/images/4.svg" alt="" className="footer-iimg" />
                <div className='member'>
                    <h1 className="main-text"> Be a  member of our community 🎉</h1>
                    <p className="secondary-text"> We’d make sure you’re always first to know what’s happening on Vasiti—thus, the world.</p>
                </div>
            </div>

            <diiv className="bottom">
                <ul>
                    <li>Company</li>
                    <li>About Us</li>
                    <li>Term of service</li>
                    <li>Privacy Policy</li>
                    <li>Press</li>
                </ul>
                <ul>
                    <li>Products</li>
                    <li>About Us</li>
                    <li>Term of service</li>
                    <li>Privacy Policy</li>
                    <li>Press</li>
                </ul>
                <ul>
                    <li>Careers</li>
                    <li>About Us</li>
                    <li>Term of service</li>
                    <li>Privacy Policy</li>
                    <li>Press</li>
                </ul>
                <ul>
                    <li>Get in touch</li>
                    <li>About Us</li>
                    <li>Term of service</li>
                    <li>Privacy Policy</li>
                    <li>Press</li>
                </ul>
                <div className="social-media">
                    <p>Join our community</p>
                    <div className="icons">
                        <Icon background='red'>
                            <FontAwesomeIcon icon={faCoffee} />
                        </Icon>
                    
                    <FontAwesomeIcon icon={faCoffee} />
                    <FontAwesomeIcon icon={faCoffee} />
                    </div>
                    <p>Email Newsletter</p>
                </div>
            </diiv>

        </div>
    )
}

export default Footer
