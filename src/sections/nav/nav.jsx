import React from 'react'
import './nav.scss'
const Nav = () => {
    return (
        <div className='nav'>
            <ul>
                <li>MARKETPLACE</li>
                <li>WHOLESALE CENTER</li>
                <li>SELLER CENTER</li>
                <li>SERVICES</li>
                <li>INTERNSHIPS</li>
                <li>EVENTS</li>
            </ul>
        </div>
    )
}

export default Nav
