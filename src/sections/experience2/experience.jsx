import React, { useContext } from 'react'
import { Context } from '../../store';
import './experience.scss'


const Experience2 = () => {
    const [state, dispatch] = useContext(Context);
    return (
        <div className='experience exp2'>
            <div className="experince-text">
                <h1>Josiah’s Experience</h1>
                <button>vendor</button>
                <p> I had the best experience shopping with vasiti. Usability of the website was great, very good customer service, an all round great experience. I would definately be coming back! I had the best experience shopping with vasiti. Usability of the website was great, very good customer service, an all round great experience. I would definately be coming back!</p>
                <a style={{cursor:"pointer"}} onClick={() => dispatch({type:'TOGGLE_MODAL'})} className='share'>Share your own story!</a>
                <img src="/images/stroke.svg" alt="skskk"/>
            </div>
            <div className="experience-image-container">        
                <div className='circle'></div>
                <div className="image">
                    <img src="/images/3.svg" alt="" />
                </div>
            </div>
        </div>
    )
}

export default Experience2
