import React from 'react'
import './hero.scss'

const Hero = () => {
    return (
        <div className='hero'>
            <div className="hero-text">
                <h1>Amazing Experiences from Our Wonderful Customers</h1>
                <p>Here is what customers and vendors are saying about us, you can share your stories with us too.</p>
            </div>
            <div className="hero-image-container">
                <div className='circle'></div>
                <div className="image">
                    <img src="/images/1.svg" alt=""/>
                </div>
            </div>
        </div>
    )
}

export default Hero
