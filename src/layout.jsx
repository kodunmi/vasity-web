import { gql, useMutation } from '@apollo/client'
import React, { useContext, useState } from 'react'
import Button from './components/button/button'
import Floating from './components/floatingButton/floating'
import Input from './components/input/input'
import Modal from './components/modal/modal'
import Spinner from './components/spinner/spinner'
import Success from './components/success/success'
import Footer from './sections/footer/footer'
import Header from './sections/header/header'
import Nav from './sections/nav/nav'
import { Context } from './store'

const ADD_EXP =  gql`
 mutation($firstname: String! $lastname: String! $type: String! $location: String $story: String! $image: Upload){
    addExp( firstname: $firstname lastname: $lastname type: $type location: $location story: $story image: $image){
        firstname
    }
 }
`

const Layout = ({ children }) => {
    const [state, dispatch] = useContext(Context);
    const [firstname, setfirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [location, setLocation] = useState('')
    const [story, setStory] = useState('')
    const [type, setType] = useState('')
    const [image, setImage] = useState(undefined)
    const [addexp, { loading, error }] = useMutation(ADD_EXP)
    
    function handlesubmit() {
        console.log(firstname, lastname, location, story, type, image);

        // const formData = new FormData();

        // formData.append('File', image);
        
        // console.log(formData);

        addexp({ variables: {firstname: firstname, lastname: lastname, type: type, story: story, location: location , image: image} }).then(data => {

            dispatch({type:"TOGGLE_MODAL"})
            dispatch({type:"TOGGLE_SUCCESS_MODAL"})
        }).catch(e => {
            console.log(e);
        })
    }

    return (
        <>
            <Modal>
                <h2 style={{ textAlign: 'center', marginBottom: '15px' }}>Share your amazing story!</h2>

                <p style={{color:'red', marginBottom:'10px'}}>{firstname == '' || lastname == '' || type == '' || story == '' ? 'fill all required fields' : '' }</p> 
                <Input text='Upload your Picture' type='file' onChange={file => setImage(file)} />

                <div style={{ display: 'grid', width: '100%', marginTop: '20px', gridTemplateColumns: 'repeat(2, 1fr)', gridGap: '5px' }}>
                    <Input text='First Name' type='text' onChange={e => setfirstname(e.target.value)} />
                    <Input text='Last Name' type='text' onChange={e => setLastname(e.target.value)} />
                </div>
                <Input onChange={e => setStory(e.target.value)} text='Share your story' type='textarea' />
                <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '20px', fontSize: '14px', marginBottom: '20px' }}>
                    <p style={{ fontSize: '14px' }}>What did you interact with Vasiti as?</p>
                    <div>
                        <input type="radio" id="male" name="type" value="vendor" onChange={e => setType(e.target.value)} />
                        <label style={{ marginRight: '10px', marginLeft: '10px' }} for="male">Vendor</label>
                        <input type="radio" id="female" name="type" value="customer" onChange={e => setType(e.target.value)} />
                        <label style={{ marginLeft: '10px' }} for="female">Customer</label>
                    </div>
                </div>
                <Input onChange={e => setLocation(e.target.value)} type='text' text='City or Higher Institution (for Students)' />
                <div style={{ marginTop: '20px' }}>
                    <Button disable={firstname == '' || lastname == '' || type == '' || story == ''} onclick={handlesubmit} type='submit' color='white' size='large' background='primary'>{loading ? <Spinner color='white' /> : "Share your story!"}</Button>
                </div>



            </Modal>
            <Success>
                <div style={{ padding: '40px', borderRadius: '50%', backgroundColor: "#FFF8F5", height: '150px', width: '155px', verticalAlign: 'middle', marginBottom: '20px', marginLeft: '50%', transform: 'translate(-50%, 0%)' }}>
                    <img src="/images/success.png" alt="" />
                </div>
                <h2 className='mb1'>Thank you for sharing your story!</h2>
                <p className='success-text'>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum repudiandae consequuntur culpa od Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, quod!</p>
                <div style={{ marginTop: "20px", textAlign: 'center' }}>
                    <Button onclick={() => dispatch({ type: 'TOGGLE_SUCCESS_MODAL' })} color='white' size='xlarge' background='primary'>close</Button>
                </div>
            </Success>
            <Floating onClick={() => dispatch({ type: 'TOGGLE_MODAL' })} color='#FF5C00' text='share story' />
            <Header />
            <Nav />
            {children}
            <Footer />
        </>
    )
}

export default Layout
