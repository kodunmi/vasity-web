import Header from "./sections/header/header";
import Nav from "./sections/nav/nav"
import Footer from './sections/footer/footer'
import Layout from "./layout";
import Home from "./pages/home/home";
import { Switch, Route } from 'react-router-dom'
import About from "./pages/about";
import Store from "./store";
import Stories from "./pages/home/stories";


function App() {
  return (
    <Store>
      <Layout>
        <Switch>
          <Route exact={true} path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/stories" component={Stories}/>
        </Switch>
      </Layout>
    </Store>

  );
}

export default App;
