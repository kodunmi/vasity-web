import React, { useContext, useState } from 'react'
import Button from '../../components/button/button'
import Card from '../../components/card/card'
import Input from '../../components/input/input'
import Modal from '../../components/modal/modal'
import Experience from '../../sections/experience/experience'
import Experience2 from '../../sections/experience2/experience'
import Hero from '../../sections/hero/hero'
import './home.scss'
import Spinner from '../../components/spinner/spinner'

import { useQuery, gql, useMutation } from '@apollo/client';
import { Context } from '../../store'
import Success from '../../components/success/success'



const GET_EXPERENCES = gql`
query{
    allExp{
        firstname
        lastname
        location
        image
        type
        story
    }
}
`


const Home = ({history, match}) => {

    
    const {data, loading: loadingexp, error: geterror } = useQuery(GET_EXPERENCES,
        {
            pollInterval: 500
        })

    

    // const [show, setShow] = useState(false);

    return (
        <div>
            <Hero />
            <Experience />

            <div className="section-paddiing">
                <div className='exp-container'>
                    {
                        loadingexp ? <Spinner color='white'/> :
                        
                        data.allExp.map(({image, firstname, lastname, location, type, story}, index) =>{
                            
                                if (index <= 5) {
                                   return( <Card name={`${firstname} ${lastname}`} image={image ? image : '/images/bank.jpg'} location={location} type={type} experience={story} />
                                   )
                                }
                            }
                        )
                    }
                </div>
                <div style={{cursor:"pointer"}} onClick={() => history.push(`/stories`)} className='view-all'>view all stories</div>
            </div>
            <Experience2 />
            <div className="section-paddiing bg-white  ">
                <div className='exp-container mb1mb1'>
                {
                        loadingexp ? <Spinner color='white'/> :
                        
                        data.allExp.map(({image, firstname, lastname, location, type, story}, index) =>{
                            
                                if (index > 5 && index <= 11) {
                                   return( <Card name={`${firstname} ${lastname}`} image={image ? image : '/images/bank.jpg'} location={location} type={type} experience={story} />
                                   )
                                }
                            
                        

                        }
                        )
                    }
                </div>

                <div style={{cursor:"pointer"}} onClick={() => history.push(`/stories`)} className='view-all'>view all stories</div>
                

            </div>
        </div>
    )
}

export default Home
