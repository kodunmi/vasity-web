import { gql, useQuery } from '@apollo/client'
import React from 'react'
import Card from '../../components/card/card'
import Spinner from '../../components/spinner/spinner'


const GET_EXPERENCES = gql`
query{
    allExp{
        firstname
        lastname
        location
        image
        type
        story
    }
}
`

const Stories = () => {

    const { data, loading: loadingexp, error: geterror } = useQuery(GET_EXPERENCES,
        {
            pollInterval: 500
        })
    return (
        <div className='section-paddiing'>
            <div className='exp-container'>
                {
                    loadingexp ? <Spinner color='white' /> :

                        data.allExp.map(({ image, firstname, lastname, location, type, story }, index) => <Card name={`${firstname} ${lastname}`} image={image ? image : '/images/bank.jpg'} location={location} type={type} experience={story} />)
                }
            </div>
        </div>
    )
}

export default Stories
