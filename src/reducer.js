const Reducer = (state, action) => {
    switch (action.type) {
        case 'TOGGLE_MODAL':
            return {
                ...state,
                show: !state.show
            };
        case 'TOGGLE_SUCCESS_MODAL':
            return { 
                ...state,
                success: !state.success
            }
        default:
            return state;
    }
};

export default Reducer;