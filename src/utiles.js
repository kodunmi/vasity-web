export const convertToBase64 =  (file) => {
  return new Promise((resolve, reject)  => {

      var reader = new FileReader();

  // Convert the file to base64 text
  var con = reader.readAsDataURL(file)

  reader.onload = () => {

      resolve(reader.result)
  }
  reader.onerror = error => reject(error)

  })
}